# Temp storage

This module provides temporary storage for arbitrary data. So you can save
pretty much anything to that storage, use it as you wish and don't bother
about the cleanup.

Keep in mind that this module does not provide any UI and should be used as
helper module by developers.

## Usage examples

1. Save data to temporary storage:
```php
$entity = entity_create('temp_storage', [
  'name' => 'my_module__daily_report',
  'data' => drupal_json_encode($report),
];
$entity->save();
```
Here we save some array to storage named `my_module__daily_report`. As you can
see we should serialize or encode to json our data before saving.

Keep in mind that this record will be automatically removed from the storage
after 1 year (by default).

2. Advanced data saving:
```php
$entity = entity_create('temp_storage', [
  'name' => 'my_module__daily_report',
  'entity_type' => 'node',
  'entity_id' => 123,
  'created' => time() - 60 * 60 * 24,
  'expired' => time() + 60 * 60 * 24 * 30,
  'data' => drupal_json_encode($report),
];
$entity->save();
```
In this example our report is related to some entity (node), so it's possible
to query items related to that entity now. Also created and expired dates are
explicitly set (this record will be removed after 1 month).

3. Load a single record from storage:
```php
$entity = entity_load_single('temp_storage', 543);
$data = drupal_json_decode($entity->data);
```

4. Load multiple records:
```php
$result = entity_load('temp_storage', FALSE, [
  'name' => 'my_module__daily_report',
  'entity_type' => 'node',
  'entity_id' => 123,
]);
```

5. Advanced way to load multiple records:
```php
$query = db_select('temp_storage', 'ts');
$query->fields('ts');
$query->condition('ts.name', 'my_module__daily_report');
$query->condition('ts.created', time() - 60 * 60 * 24, '>=');
$query->condition('ts.created', time(), '<=');
$query->orderBy('ts.created');

$result = $query->execute()->fetchAllAssoc('id');
```
Here we load all records from `my_module__daily_report` storage for the
last day.
