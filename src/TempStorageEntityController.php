<?php

/**
 * Extends EntityAPIController for Temp Storage entity.
 */
class TempStorageEntityController extends EntityAPIController {

  /**
   * Default data storage expiration (sec) (1 year).
   *
   * @var int
   */
  const DATA_EXPIRATION = 31536000;

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    // Set default values for created and expired dates.
    if (empty($entity->created)) {
      $entity->created = time();
    }
    if (empty($entity->expired)) {
      $entity->expired = $entity->created + self::DATA_EXPIRATION;
    }

    return parent::save($entity, $transaction);
  }

  /**
   * Removes expired records from temp storage.
   *
   * @param int $timestamp
   *   Expiration date. By default, current time is used.
   *
   * @return int
   *   The number of rows affected by the delete query.
   */
  public function deleteExpired(int $timestamp = 0) {
    if (!$timestamp) {
      $timestamp = time();
    }

    $query = db_delete($this->entityInfo['base table'])
      ->condition('expired', $timestamp, '<');

    return $query->execute();
  }

}
